defmodule KVstore.KVstoreTest do
  use ExUnit.Case
  use Plug.Test

  alias KVstore.{Router, Storage}

  @opts Router.init([])

  setup do
    Storage.delete_all()

    Storage.insert("zero", "0000", 1000)
    Storage.insert("second", "2222", 1000)

    {:ok, %{}}
  end

  test "should create key" do
    conn =
      conn(:post, "/api", key: "first", value: 1, ttl: "9999")
      |> Router.call(@opts)

    assert conn.resp_body == "key: first, data:1 ttl:9999"
  end

  test "should return all keys" do
    conn =
      conn(:get, "/api")
      |> Router.call(@opts)

      assert conn.resp_body =~ "key: zero, data:0000 ttl:"
      assert conn.resp_body =~ "key: second, data:2222 ttl:"
  end

  test "should return one key" do
    conn =
      conn(:get, "/api/zero")
      |> Router.call(@opts)

      assert conn.resp_body =~ "key: zero, data:0000 ttl:"
      refute conn.resp_body =~ "key: second, data:2222 ttl:"
  end

  test "should update key" do
    conn =
      conn(:put, "/api/second", value: 99999, ttl: "9999")
      |> Router.call(@opts)

    assert conn.resp_body == "key: second, data:99999 ttl:9999"
  end

  test "should delete key" do
    conn =
      conn(:delete, "/api/second")
      |> Router.call(@opts)

    assert conn.resp_body == "key: 'second' was deleted"
  end
end
