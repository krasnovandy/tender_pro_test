
use Mix.Config

config :kvstore,
  storage_path: "./storage_#{Mix.env}.db"

config :kvstore, Cowboy,
  port: 9000

import_config "#{Mix.env}.exs"
