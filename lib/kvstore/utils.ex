defmodule KVstore.Utils do
  @moduledoc """
    Utils
  """

  @storage_name KVstore.Storage

  def delete_dets(key) do
    result = :dets.delete(@storage_name, key)
    :dets.sync(@storage_name)

    result
  end

  def get_all_dets() do
    select_all = [{:"$1", [], [:"$1"]}]
    :dets.select(@storage_name, select_all)
  end

  def format_data({key, value, ttl}) do
    "key: #{key}, data:#{value} ttl:#{ttl}"
  end

  def get_data(key) do
    case KVstore.Storage.get(key) do
      nil ->
        raise(ArgumentError, message: "data not found")

      res ->
        res
    end
  end
end
