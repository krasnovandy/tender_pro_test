defmodule KVstore.Router do
  @moduledoc """
  App router
  """
  use Plug.Router
  use Plug.ErrorHandler

  import Plug.Conn
  import KVstore.Utils, only: [format_data: 1, get_data: 1]

  alias KVstore.Storage

  plug :match
  plug :dispatch

  get "api/" do
    res =
      Storage.get_all
      |> Enum.map(&format_data/1)
      |> Enum.join("<br>")

    put_resp_content_type(conn, "text/html")
    |> send_resp(200, res)
  end

  get "api/:key" do
    data = get_data(key)

    send_resp(conn, 200, format_data(data))
  end

  post "api/" do
    %{"key" => key, "value" => value, "ttl" => ttl} = conn.params
    ttl = String.to_integer(ttl)

    {status, message} =
      Storage.get(key)
      |> case do
        nil ->
          Storage.insert(key, value, ttl)
          {201, format_data({key, value, ttl})}

        _ ->
          {400, "Key already exist"}
      end

    send_resp(conn, status, message)
  end

  put "api/:key" do
    %{"key" => key, "value" => value, "ttl" => ttl} = conn.params
    ttl = String.to_integer(ttl)

    {status, message} =
      Storage.get(key)
      |> case do
        nil ->
          {400, "Key doesn`t exist"}

        _ ->
          Storage.insert(key, value, ttl)
          {201, format_data({key, value, ttl})}
      end

    send_resp(conn, status, message)
  end

  delete "api/:key" do
    Storage.delete(key)

    send_resp(conn, 204, "key: '#{key}' was deleted")
  end

  match _ do
    send_resp(conn, 404, "something was wrong")
  end
end
