defmodule KVstore.Storage do
  @moduledoc """
  Storage
  """
  use GenServer
  require Logger
  import KVstore.Utils, only: [delete_dets: 1, get_all_dets: 0]

  @name __MODULE__

  def get_name(), do: @name

  def get_storage_name(), do: @name

  def start_link(opts) do
    GenServer.start_link(@name, opts, name: @name)
  end

  def init(_) do
    file_path = Application.get_env(:kvstore, :storage_path)
    :dets.open_file(@name, file: to_charlist(file_path), type: :set, ram_file: true)
    |> case do
      {:ok, _} ->
        now = System.system_time(:millisecond)
        {_, state} =
          get_all_dets()
          |> Enum.map_reduce(%{timer_refs: %{}}, fn {key, _data, ttl}, acc ->
            acc =
              if (diff = (ttl - now)) > 0 do
                get_state(acc, key, diff)
              else
                delete_dets(key)

                acc
              end

            {key, acc}
          end)

        {:ok, state}

      {:error, res} ->
        Logger.warn("can not open table, reason: #{res}")
        {:stop, res}
    end
  end

  defp get_state(state, key, ttl) do
    timer_ref = Process.send_after(@name, {:delete, key}, ttl)

    new_timer_refs =
    state
    |> Map.get(:timer_refs)
    |> Map.put(key, timer_ref)

    state |> Map.put(:timer_refs, new_timer_refs)
  end

  def insert(key, value, ttl) do
    GenServer.call(@name, {:insert, key, value, ttl})
  end

  def get(key) do
    GenServer.call(@name, {:get, key})

    case :dets.lookup(@name, key) do
      [] -> nil

      [res] ->
        res
    end
  end

  def delete_all do
    # 4 tests only
    :dets.delete_all_objects(@name)
    :dets.sync(@name)
  end

  def get_all do
    GenServer.call(@name, {:get_all})
  end

  def delete(key) do
    GenServer.call(@name, {:delete, key})
  end

  defp cancel_timer(key, state) do
    case timer_ref = state.timer_refs[key] do
      nil -> nil
      _ -> Process.cancel_timer(timer_ref)
    end
  end

  def handle_info({:delete, key}, state) do
    result = delete_dets(key)

    {:noreply, result, state}
  end

  def handle_call({:delete, key}, _from, state) do
    result = delete_dets(key)

    {:reply, result, state}
  end

  def handle_call({:insert, key, value, ttl}, _from, state) do
    ttl_end = System.system_time(:millisecond) + ttl

    cancel_timer(key, state)

    state = get_state(state, key, ttl)

    :ok = :dets.insert(@name, {key, value, ttl_end})
    :dets.sync(@name)

    {:reply, value, state}
  end

  def handle_call({:get, key}, _from, state) do
    result =
      case :dets.lookup(@name, key) do
        [] -> nil

        [res] ->
          res
      end

    {:reply, result, state}
  end

  def handle_call({:get_all}, _from, state) do
    result = get_all_dets()

    {:reply, result, state}
  end
end
